package com.inforscience.requisition;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import com.inforscience.requisition.model.*;
import com.inforscience.requisition.net.WebService;

import java.util.ArrayList;

public class HomeActivity extends Activity {
    private SQLiteDatabase database;
    private ShopActivity shopActivity;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        Button updateButton = (Button)findViewById(R.id.update_catalogs_button);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                DBManager.clean(database);
                Spinner clientSpinner = (Spinner)shopActivity
                                            .findViewById(R.id.client_list);
                WebService webService = new WebService();
                try {
                    ArrayList<Client> clients = webService.queryClients();
                    ArrayAdapter<Client> adapter =
                            new ArrayAdapter<Client>(getApplicationContext(),
                                    android.R.layout.select_dialog_item,
                                    clients);

                    clientSpinner.setAdapter(adapter);
                    clientSpinner.setBackgroundColor(Color.GRAY);

                    int n = clients.size();
                    for (int i = 0; i < n; i++) {
                        long id = DBManager.insertClient(clients.get(i), database);
                        Log.w("CLIENT ID", id + "");
                    }

                } catch (Exception e) {
                    Log.e("FAIL", e.toString());
                }


                Spinner productSpinner = (Spinner)shopActivity
                                            .findViewById(R.id.product_list);
                try {
                    ArrayList<Product> products = webService.queryProducts();
                    ArrayAdapter<Product> adapter =
                            new ArrayAdapter<Product>(getApplicationContext(),
                                    android.R.layout.select_dialog_item,
                                    products);

                    int n = products.size();
                    for (int i = 0; i < n; i++) {
                        long id = DBManager.insertProduct(products.get(i), database);
                        Log.w("PRODUCT ID", id + "");
                    }

                    productSpinner.setAdapter(adapter);
                    productSpinner.setBackgroundColor(Color.GRAY);
                } catch (Exception e) {
                    Log.e("FAIL", e.toString());
                }
            }
        });

        Button putButton = (Button)findViewById(R.id.put_registers_button);
        putButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                WebService webService = new WebService();
                ArrayList<Request> R = DBManager.selectRequests(database);
                ArrayList<Detail> D = DBManager.selectDetails(database);
                try {
                    webService.putRegisters(R, D);
                } catch (Exception e) {
                    Log.e("PUT REGISTER ERROR", e.toString());
                }
            }
        });

    }


    public SQLiteDatabase getDatabase()
    {
        return database;
    }

    public void setDatabase(SQLiteDatabase database)
    {
        this.database = database;
    }

    public ShopActivity getShopActivity()
    {
        return shopActivity;
    }

    public void setShopActivity(ShopActivity shopActivity)
    {
        this.shopActivity = shopActivity;
    }
}


