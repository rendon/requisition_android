package com.inforscience.requisition.net;


import android.util.Log;
import com.inforscience.requisition.model.Client;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.ArrayList;

public class ClientXMLHandler extends DefaultHandler {
    private ArrayList<Client> clients;
    private Client tempClient;
    private String value;

    public ClientXMLHandler()
    {
        clients = new ArrayList<Client>();
    }

    public ArrayList<Client> getClients()
    {
        return clients;
    }

    @Override
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException
    {
        value = "";
        if (qName.equalsIgnoreCase("item"))
            tempClient = new Client();
    }

    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException
    {
        value = new String(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException
    {
        if (qName.equalsIgnoreCase("item")) {
            clients.add(tempClient);
        } else if (qName.equalsIgnoreCase("Idclient")) {
            tempClient.setIdClient(Integer.parseInt(value));
        } else if (qName.equalsIgnoreCase("Name")) {
            tempClient.setName(value);
        } else if (qName.equalsIgnoreCase("MiddleName")) {
            tempClient.setMiddleName(value);
        } else if (qName.equalsIgnoreCase("LastName")) {
            tempClient.setLastName(value);
        } else if (qName.equalsIgnoreCase("Address")) {
            tempClient.setAddress(value);
        }
    }

}
