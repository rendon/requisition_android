package com.inforscience.requisition.net;


import android.util.Log;
import com.inforscience.requisition.model.Client;
import com.inforscience.requisition.model.Product;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;
import java.util.ArrayList;

public class XMLParser {
    private XMLParser()
    {
    }

    public static ArrayList<Client> parseClients(InputStream is)
    {
        ArrayList<Client> clients = null;
        try {
            XMLReader reader = SAXParserFactory.newInstance()
                    .newSAXParser().getXMLReader();

            ClientXMLHandler clientXMLHandler = new ClientXMLHandler();
            reader.setContentHandler(clientXMLHandler);
            reader.parse(new InputSource(is));
            clients = clientXMLHandler.getClients();
        } catch (Exception e) {
            Log.d("PARSING CLIENTS", e.toString());
        }

        return clients;
    }

    public static ArrayList<Product> parseProducts(InputStream is)
    {
        ArrayList<Product> products = null;
        try {
            XMLReader reader = SAXParserFactory.newInstance()
                    .newSAXParser().getXMLReader();

            ProductXMLHandler productXMLHandler = new ProductXMLHandler();
            reader.setContentHandler(productXMLHandler);
            reader.parse(new InputSource(is));
            products = productXMLHandler.getProducts();
        } catch (Exception e) {
            Log.d("PARSING CLIENTS", e.toString());
        }

        return products;
    }
}

