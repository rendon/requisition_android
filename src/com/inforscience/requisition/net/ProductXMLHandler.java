package com.inforscience.requisition.net;

import com.inforscience.requisition.model.Product;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

public class ProductXMLHandler extends DefaultHandler {
    private ArrayList<Product> products;
    private Product tempProduct;
    private String value;

    public ProductXMLHandler() {
        products = new ArrayList<Product>();
    }

    public ArrayList<Product> getProducts()
    {
        return products;
    }

    @Override
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException
    {
        value = "";
        if (qName.equalsIgnoreCase("item"))
            tempProduct = new Product();
    }

    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException
    {
        value = new String(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException
    {
        if (qName.equalsIgnoreCase("item")) {
            products.add(tempProduct);
        } else if (qName.equalsIgnoreCase("IdProduct")) {
            tempProduct.setIdProduct(Integer.parseInt(value));
        } else if (qName.equalsIgnoreCase("Description")) {
            tempProduct.setDescription(value);
        }
    }

}
