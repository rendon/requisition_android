package com.inforscience.requisition.net;
import android.util.Log;
import com.inforscience.requisition.model.Client;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import com.inforscience.requisition.model.Detail;
import com.inforscience.requisition.model.Product;
import com.inforscience.requisition.model.Request;


public class WebService {
    public static final String WS_URL =
            "http://192.168.1.200/request_ws/index.php";
    public static final String MSG_GET_ALL_CLIENTS =
          "<?xml version='1.0'?> <soapenv:Envelope xmlns:soapenv="
        + "'http://schemas.xmlsoap.org/soap/envelope/' xmlns:req='Request'> "
        + "<soapenv:Header/> <soapenv:Body> <req:GetAllClients/> "
        + "</soapenv:Body> </soapenv:Envelope>";

    public static final String MSG_GET_ALL_PRODUCTS =
          "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/"
        + "envelope/' xmlns:req='Request'> <soapenv:Header/> <soapenv:Body> "
        + "<req:GetAllProducts/> </soapenv:Body> </soapenv:Envelope>";


    public WebService()
    {
    }

    public ArrayList<Client> queryClients() throws Exception
    {
        URL url = new URL(WS_URL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type:", "text/xml");

        conn.setDoOutput(true);
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.writeBytes(MSG_GET_ALL_CLIENTS);
        out.flush();
        out.close();

        return XMLParser.parseClients(conn.getInputStream());
    }


    public ArrayList<Product> queryProducts() throws Exception
    {
        URL url = new URL(WS_URL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type:", "text/xml");

        conn.setDoOutput(true);
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.writeBytes(MSG_GET_ALL_PRODUCTS);
        out.flush();
        out.close();

        return XMLParser.parseProducts(conn.getInputStream());
    }

    public void putRegisters(ArrayList<Request> R,
                             ArrayList<Detail> D) throws Exception
    {

        String header =
                "<soapenv:Envelope "
                + "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'"
                + " xmlns:req='Request'> <soapenv:Header/> <soapenv:Body> "
                + " <req:PutRegisters>";

        String footer  = "</req:PutRegisters> "
                + "</soapenv:Body> "
                + "</soapenv:Envelope>";

        String requests = "\n<RequestItems>\n";

        int n = R.size();
        for (int i = 0; i < n; i++) {
            Request r = R.get(i);
            requests += "<item>";
            requests += "<IdRequest>" + r.getIdRequest() + "</IdRequest>";
            requests += "<IdClient>" + r.getIdClient()+ "</IdClient>";
            requests += "<RequestDate>" + r.getRequestDate() + "</RequestDate>";

            requests += "</item>" + "\n";
        }
        requests += "</RequestItems>";

        String details = "<DetailItems>" + "\n";
        n = D.size();
        for (int i = 0; i < n; i++) {
            Detail d = D.get(i);
            details += "<item>";
            details += "<IdDetail>" + d.getIdDetail() + "</IdDetail>";
            details += "<IdRequest>" + d.getIdRequest() + "</IdRequest>";
            details += "<IdProduct>" + d.getIdProduct() + "</IdProduct>";
            details += "<Amount>" + d.getAmount() + "</Amount>";
            details += "</item>\n";
        }
        details += "</DetailItems>";


        String message = header + requests + "\n" + details + "\n" + footer;

        URL url = new URL(WS_URL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type:", "text/xml");

        conn.setDoOutput(true);
        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        out.writeBytes(message);
        out.flush();
        out.close();

        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line, response = "";
        while ((line = reader.readLine()) != null) {
            response += line;
        }

        Log.w("RESPONSE", response);
    }
}

