package com.inforscience.requisition;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.inforscience.requisition.model.DBManager;

import java.util.ArrayList;

public class SearchActivity extends Activity {

    private SQLiteDatabase database;
    //private ArrayAdapter<String> requestAdapter;
    private  RequestAdapter requestAdapter;
    private ArrayList<String> requests;

    @Override
    public void onCreate(Bundle savedInstanceBundle)
    {
        super.onCreate(savedInstanceBundle);
        setContentView(R.layout.search);

        ListView listView = (ListView)findViewById(R.id.request_list);
        requests = new ArrayList<String>();
        requestAdapter = new RequestAdapter(this, requests);
        listView.setAdapter(requestAdapter);
        Button refreshButton = (Button)findViewById(R.id.refresh_query_button);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                ArrayList<String> items = DBManager.queryRequests(database);
                requests.clear();
                for (int i = 0; i < items.size(); i++) {
                    Log.w("ITEM LIST", items.get(i));
                    requests.add(items.get(i));
                }

                requestAdapter.notifyDataSetChanged();
            }
        });
    }

    public SQLiteDatabase getDatabase()
    {
        return database;
    }

    public void setDatabase(SQLiteDatabase database)
    {
        this.database = database;
    }
}

class RequestAdapter extends ArrayAdapter<String> {
    private Context context;
    private ArrayList values;
    public RequestAdapter(Context context, ArrayList<String> values)
    {
        super(context, R.id.request_list, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        String item = (String)values.get(position);
        TextView textView = new TextView(context);
        textView.setText((String) values.get(position));
        textView.setTypeface(Typeface.MONOSPACE);
        textView.setTextSize(16);

        int tc = Color.argb(255, 62, 183, 227);
        if (item.contains(":"))
            textView.setTextColor(tc);

        return textView;
    }
}

