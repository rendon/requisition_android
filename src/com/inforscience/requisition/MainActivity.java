package com.inforscience.requisition;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import android.widget.TabHost.TabSpec;
import com.inforscience.requisition.model.DBManager;
import com.inforscience.requisition.net.WebService;

import java.io.*;

public class MainActivity extends TabActivity {
    private SQLiteDatabase database;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        TabHost tabHost = getTabHost();
        Resources resources = getResources();

        Intent homeIntent = new Intent().setClass(this, HomeActivity.class);
        TabSpec homeTab = tabHost
                .newTabSpec("Home")
                .setIndicator("", resources.getDrawable(R.drawable.icon_home))
                .setContent(homeIntent);


        Intent shopIntent = new Intent().setClass(this, ShopActivity.class);
        TabSpec shopTab = tabHost
                .newTabSpec("Shop")
                .setIndicator("", resources.getDrawable(R.drawable.icon_shop))
                .setContent(shopIntent);

        Intent searchIntent = new Intent().setClass(this, SearchActivity.class);
        TabSpec searchTab = tabHost
                .newTabSpec("Search")
                .setIndicator("", resources.getDrawable(R.drawable.icon_search))
                .setContent(searchIntent);

        tabHost.addTab(homeTab);
        tabHost.addTab(shopTab);
        tabHost.addTab(searchTab);



        database = openOrCreateDatabase(DBManager.DB_NAME,
                    Context.MODE_PRIVATE, null);
        DBManager.initialize(database);

        tabHost.setCurrentTab(2);
        SearchActivity searchActivity = (SearchActivity)getLocalActivityManager()
                .getCurrentActivity();
        searchActivity.setDatabase(database);

        tabHost.setCurrentTab(1);
        ShopActivity shopActivity = (ShopActivity)getLocalActivityManager()
                                        .getCurrentActivity();
        shopActivity.setDatabase(database);
        shopActivity.populateCatalogs();


        tabHost.setCurrentTab(0);
        HomeActivity homeActivity = (HomeActivity)getLocalActivityManager()
                                        .getCurrentActivity();
        homeActivity.setDatabase(database);
        homeActivity.setShopActivity(shopActivity);

    }


    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (database != null) {
            database.close();
        }
    }

}

