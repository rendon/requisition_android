package com.inforscience.requisition.model;

public class Request {
    private int idRequest;
    private int idClient;
    private String requestDate;

    public Request() { }

    public Request(int idRequest, int idClient, String requestDate)
    {
        this.idRequest = idRequest;
        this.idClient = idClient;
        this.requestDate = requestDate;
    }

    public int getIdRequest()
    {
        return idRequest;
    }

    public void setIdRequest(int idRequest)
    {
        this.idRequest = idRequest;
    }

    public int getIdClient()
    {
        return idClient;
    }

    public void setIdClient(int idClient)
    {
        this.idClient = idClient;
    }

    public String getRequestDate()
    {
        return requestDate;
    }

    public void setRequestDate(String requestDate)
    {
        this.requestDate = requestDate;
    }
}
