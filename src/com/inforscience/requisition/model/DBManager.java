package com.inforscience.requisition.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class DBManager {
    public static final String DB_NAME = "pedidos";

    private static final String DB_CREATE_CLIENT =
            "CREATE TABLE IF NOT EXISTS Client (IdClient INTEGER PRIMARY KEY, "
                    + "Name VARCHAR(20), MiddleName VARCHAR(20), LastName VARCHAR(20), "
                    + "Address VARCHAR(100))";

    private static final String UNIQUE_FULL_NAME_CONSTRAINT =
            "CREATE UNIQUE INDEX IF NOT EXISTS unique_fullname ON "
                    + "Client(Name, MiddleName, LastName, Address)";

    private static final String DB_CREATE_REQUEST =
            "CREATE TABLE IF NOT EXISTS Request(IdRequest INTEGER PRIMARY KEY "
                    + "AUTOINCREMENT, IdClient INTEGER, RequestDate DATETIME, "
                    + "FOREIGN KEY (IdClient) REFERENCES Client (IdClient))";

    private static final String UNIQUE_REQUEST_CONSTRAINT =
            "CREATE UNIQUE INDEX IF NOT EXISTS unique_request "
                    + "ON Request(IdRequest, IdClient)";

    private static final String DB_CREATE_PRODUCT =
            "CREATE TABLE IF NOT EXISTS Product(IdProduct "
                    + "INTEGER PRIMARY KEY, Description VARCHAR(100))";

    private static final String DB_CREATE_DETAIL =
            "CREATE TABLE IF NOT EXISTS Detail(IdDetail INTEGER PRIMARY "
                    + "KEY AUTOINCREMENT, IdRequest INTEGER, IdProduct INTEGER, "
                    + "Amount INTEGER, FOREIGN KEY (IdRequest) REFERENCES Request "
                    + "(IdRequest), FOREIGN KEY(IdProduct) REFERENCES Product(IdProduct))";

    private static final String UNIQUE_DETAIL_CONSTRAINT =
            "CREATE UNIQUE INDEX IF NOT EXISTS unique_detail "
                    + "ON Detail(IdDetail, IdRequest)";

    public DBManager()
    {
    }

    public static void initialize(SQLiteDatabase db)
    {
        db.execSQL(DB_CREATE_CLIENT);
        db.execSQL(DB_CREATE_PRODUCT);
        db.execSQL(DB_CREATE_REQUEST);
        db.execSQL(DB_CREATE_DETAIL);
    }

    public static long insertClient(Client client, SQLiteDatabase db)
    {
        ContentValues values = new ContentValues();
        values.put("IdClient", client.getIdClient());
        values.put("Name", client.getName());
        values.put("MiddleName", client.getMiddleName());
        values.put("LastName", client.getLastName());
        values.put("Address", client.getAddress());

        return db.insert("Client", null, values);
    }


    public static long insertProduct(Product product, SQLiteDatabase db)
    {
        ContentValues values = new ContentValues();
        values.put("IdProduct", product.getIdProduct());
        values.put("Description", product.getDescription());

        return db.insert("Product", null, values);
    }


    public static long insertRequest(Request request, SQLiteDatabase db)
    {
        ContentValues values = new ContentValues();
        values.put("IdClient", request.getIdClient());
        values.put("RequestDate", request.getRequestDate());

        return db.insert("Request", null, values);
    }

    public static long insertDetail(Detail detail, SQLiteDatabase db)
    {
        ContentValues values = new ContentValues();
        values.put("IdRequest", detail.getIdRequest());
        values.put("IdProduct", detail.getIdProduct());
        values.put("Amount", detail.getAmount());

        return db.insert("Detail", null, values);
    }

    public static ArrayList<Client> selectClients(SQLiteDatabase db)
    {
        String[] fields = new String[] {"IdClient", "Name", "MiddleName",
                "LastName", "Address"};

        Cursor r = db.query("Client", fields, null, null, null, null, null);

        ArrayList<Client> clients = new ArrayList<Client>();
        if (r.moveToFirst()) {
            do {
                Client c = new Client();
                c.setIdClient(Integer.parseInt(r.getString(0)));
                c.setName(r.getString(1));
                c.setMiddleName(r.getString(2));
                c.setLastName(r.getString(3));
                c.setAddress(r.getString(4));
                clients.add(c);
            } while (r.moveToNext());
        }

        return clients;
    }

    public static ArrayList<Request> selectRequests(SQLiteDatabase db)
    {
        String[] fields = new String[] {"IdRequest", "IdClient", "RequestDate"};
        Cursor r = db.query("Request", fields, null, null, null, null, null);

        ArrayList<Request> requests = new ArrayList<Request>();
        if (r.moveToFirst()) {
            do {
                Request request = new Request();
                request.setIdRequest(Integer.parseInt(r.getString(0)));
                request.setIdClient(Integer.parseInt(r.getString(1)));
                request.setRequestDate(r.getString(2));
                requests.add(request);
            } while (r.moveToNext());
        }

        return requests;
    }

    public static ArrayList<Product> selectProducts(SQLiteDatabase db)
    {
        String[] fields = new String[] {"IdProduct", "Description"};
        Cursor r = db.query("Product", fields, null, null, null, null, null);

        ArrayList<Product> products = new ArrayList<Product>();
        if (r.moveToFirst()) {
            do {
                Product p = new Product();
                p.setIdProduct(Integer.parseInt(r.getString(0)));
                p.setDescription(r.getString(1));
                products.add(p);
            } while (r.moveToNext());
        }

        return products;
    }

    public static ArrayList<Detail> selectDetails(SQLiteDatabase db)
    {
        String[] fields = new String[] {"IdDetail", "IdRequest",
                "IdProduct", "Amount"};
        Cursor r = db.query("Detail", fields, null, null, null, null, null);

        ArrayList<Detail> details = new ArrayList<Detail>();
        if (r.moveToFirst()) {
            do {
                Detail d = new Detail();
                d.setIdDetail(Integer.parseInt(r.getString(0)));
                d.setIdRequest(Integer.parseInt(r.getString(1)));
                d.setIdProduct(Integer.parseInt(r.getString(2)));
                d.setAmount(Integer.parseInt(r.getString(3)));
                details.add(d);
            } while (r.moveToNext());
        }

        return details;
    }

    public static void clean(SQLiteDatabase db)
    {
        db.delete("Detail", null, null);
        db.delete("Request", null, null);
        db.delete("Product", null, null);
        db.delete("Client", null, null);
    }


    public static ArrayList<String> queryRequests(SQLiteDatabase db)
    {
        Cursor r = db.rawQuery(
                  "SELECT Request.IdRequest, Client.Name, Client.MiddleName, "
                + "Client.LastName, Request.RequestDate, Detail.Amount, "
                + "Product.Description FROM Request, Client, Detail, Product "
                + "WHERE Detail.IdRequest = Request.IdRequest AND "
                + "Request.IdClient = Client.IdClient AND "
                + "Detail.IdProduct = Product.IdProduct", null);

        TreeMap<String, ArrayList<String>> map =
                new TreeMap<String, ArrayList<String>>();
        ArrayList<String> requests = new ArrayList<String>();

        if (r.moveToFirst()) {
            do {
                String key  = r.getString(0) + ": "
                            + r.getString(1) + " "
                            + r.getString(2) + " "
                            + r.getString(3) + "\n";
                String date = r.getString(4).toString();

                for (int i = 0; i < key.length(); i++) {
                    if (Character.isLetter(key.charAt(i)))
                        break;
                    date = " " + date;
                }

                key += date;

                String amount = r.getString(5);
                while (amount.length() < 4)
                    amount = " " + amount;

                String value = amount + " | " + r.getString(6);

                if (map.containsKey(key)) {
                    ArrayList<String> a = map.get(key);
                    a.add(value);
                } else {
                    ArrayList<String> a = new ArrayList<String>();
                    a.add(value);
                    map.put(key, a);
                }
            } while (r.moveToNext());
        }

        for (Map.Entry<String, ArrayList<String>> entry : map.entrySet()) {
            requests.add(entry.getKey());
            for (int i = 0; i < entry.getValue().size(); i++) {
                requests.add(entry.getValue().get(i));
            }
        }

        return requests;
    }
}
