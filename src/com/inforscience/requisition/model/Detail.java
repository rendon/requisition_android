package com.inforscience.requisition.model;

public class Detail {
    private int idDetail;
    private int idRequest;
    private int idProduct;
    private int amount;
    private String description; // An ugly work around

    public Detail()
    {
        setIdDetail(0);
        setIdRequest(0);
        setIdProduct(0);
        setAmount(0);
        setDescription("");
    }

    public Detail(int idDetail, int idRequest, int idProduct, int amount)
    {
        this.idDetail = idDetail;
        this.idRequest = idRequest;
        this.idProduct = idProduct;
        this.amount = amount;
    }

    public int getIdDetail()
    {
        return idDetail;
    }

    public void setIdDetail(int idDetail)
    {
        this.idDetail = idDetail;
    }

    public int getIdRequest()
    {
        return idRequest;
    }

    public void setIdRequest(int idRequest)
    {
        this.idRequest = idRequest;
    }

    public int getIdProduct()
    {
        return idProduct;
    }

    public void setIdProduct(int idProduct)
    {
        this.idProduct = idProduct;
    }

    public int getAmount()
    {
        return amount;
    }

    public void setAmount(int amount)
    {
        this.amount = amount;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }



    @Override
    public String toString()
    {
        return getAmount() + " |  " + getDescription();
    }
}
