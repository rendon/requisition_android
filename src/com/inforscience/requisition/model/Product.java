package com.inforscience.requisition.model;

public class Product {
    private int idProduct;
    private String description;

    public Product() { }

    public Product(int idProduct, String description)
    {
        this.idProduct = idProduct;
        this.description = description;
    }

    public int getIdProduct()
    {
        return idProduct;
    }

    public void setIdProduct(int idProduct)
    {
        this.idProduct = idProduct;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    @Override
    public String toString()
    {
        return getDescription();
    }
}
