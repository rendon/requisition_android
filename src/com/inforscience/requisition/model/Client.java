package com.inforscience.requisition.model;

public class Client {
    private int idClient;
    private String name;
    private String middleName;
    private String lastName;
    private String address;

    public Client() { };

    public Client(int idClient, String name, String middleName,
                  String lastName, String address)
    {
        this.idClient = idClient;
        this.name = name;
        this.middleName = middleName;
        this.lastName = lastName;
        this.address = address;
    }

    public int getIdClient()
    {
        return idClient;
    }

    public void setIdClient(int idClient)
    {
        this.idClient = idClient;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getMiddleName()
    {
        return middleName;
    }

    public void setMiddleName(String middleName)
    {
        this.middleName = middleName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }


    @Override
    public String toString()
    {
        return getName() + " " + getMiddleName() + " " + getLastName();
    }


}
