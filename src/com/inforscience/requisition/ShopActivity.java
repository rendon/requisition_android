package com.inforscience.requisition;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.inforscience.requisition.model.*;
import com.inforscience.requisition.net.WebService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ShopActivity extends Activity {
    private ArrayList<Detail> detailList;
    //private ArrayAdapter<Detail> detailArrayAdapter;
    private DetailAdapter detailArrayAdapter;


    private SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shop);

        detailList = new ArrayList<Detail>();
        ListView listView = (ListView)findViewById(R.id.new_items_list);
        detailArrayAdapter = new DetailAdapter(this, detailList);
        listView.setAdapter(detailArrayAdapter);

        Button addItemButton = (Button)findViewById(R.id.add_item_button);
        addItemButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                EditText editText = (EditText)findViewById(R.id.amount_text);
                Log.e("ADD ITEM", editText.getText().toString());

                int amount = 0;
                try {
                    amount = Integer.parseInt(editText.getText().toString());
                } catch (NumberFormatException e) {
                    Toast.makeText(getApplicationContext(),
                            "La cantidad no es válida.",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                Spinner products = (Spinner)findViewById(R.id.product_list);
                Product p = (Product)products.getSelectedItem();
                Log.e("ITEM ID", p.getIdProduct() + " | " + p.getDescription());

                Detail detail = new Detail();
                detail.setIdProduct(p.getIdProduct());
                detail.setAmount(amount);
                detail.setDescription(p.getDescription());
                detailList.add(detail);
                detailArrayAdapter.notifyDataSetChanged();

                editText.setText("");
            }
        });

        Button saveRequestButton = (Button)findViewById(R.id.save_request_button);
        saveRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if (detailList.isEmpty()) {
                    Toast.makeText(getApplicationContext(),
                            "No hay artículos que guardar.",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                Spinner clientSpinner = (Spinner)findViewById(R.id.client_list);
                Client client = (Client)clientSpinner.getSelectedItem();
                int idClient = client.getIdClient();
                Request request = new Request();
                request.setIdClient(idClient);
                request.setRequestDate(rightNow());
                long idRequest = DBManager.insertRequest(request, database);

                boolean ok = true;

                if (idRequest == -1) {
                    Toast.makeText(getApplicationContext(),
                            "Ocurrio error al intentar guardar el pedido.",
                            Toast.LENGTH_LONG).show();
                    ok = false;
                    return;
                }

                int n = detailList.size();
                for (int i = 0; i < n; i++) {
                    Detail detail = detailList.get(i);
                    detail.setIdRequest((int)idRequest);
                    long id = DBManager.insertDetail(detail, database);
                    if (id == -1) {
                        Toast.makeText(getApplicationContext(),
                                "ERROR AL INSERTAR DETALLE",
                                Toast.LENGTH_LONG).show();
                        ok = false;
                    }

                    Log.w("DETAIL ID", id + "");
                }

                if (ok) {
                    detailList.clear();
                    detailArrayAdapter.notifyDataSetChanged();

                    Toast.makeText(getApplicationContext(),
                            "El pedido se guardo satisfactoriamente.",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public void populateCatalogs()
    {
        ArrayList<Client> clients = DBManager.selectClients(database);
        ArrayAdapter<Client> clientAdapter =
                new ArrayAdapter<Client>(getApplicationContext(),
                        android.R.layout.select_dialog_item,
                        clients);

        Spinner clientSpinner = (Spinner)findViewById(R.id.client_list);
        clientSpinner.setAdapter(clientAdapter);
        clientSpinner.setBackgroundColor(Color.GRAY);

        Spinner productSpinner = (Spinner)findViewById(R.id.product_list);
        ArrayList<Product> products = DBManager.selectProducts(database);
        ArrayAdapter<Product> productAdapter =
                new ArrayAdapter<Product>(getApplicationContext(),
                        android.R.layout.select_dialog_item,
                        products);

        productSpinner.setAdapter(productAdapter);
        productSpinner.setBackgroundColor(Color.GRAY);
    }

    private String rightNow()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public SQLiteDatabase getDatabase()
    {
        return database;
    }

    public void setDatabase(SQLiteDatabase database)
    {
        this.database = database;
    }
}


class DetailAdapter extends ArrayAdapter<Detail> {
    private Context context;
    private ArrayList<Detail> values;
    public DetailAdapter(Context context, ArrayList<Detail> values)
    {
        super(context, R.id.request_list, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        Detail detail = values.get(position);
        String a = detail.getAmount() + "";
        while (a.length() < 4)
            a = " " + a;

        TextView textView = new TextView(context);
        textView.setText(a + " | " + detail.getDescription());
        textView.setTypeface(Typeface.MONOSPACE);
        textView.setTextSize(16);

        return textView;
    }
}
